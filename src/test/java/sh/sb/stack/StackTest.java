package sh.sb.stack;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by administrator on 17-09-14.
 */
public class StackTest {

    @Test
    public void testPush(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);
        int pop = stack.pop();
        Assert.assertTrue( stack.size() == 1);
        Assert.assertTrue( pop == 4);
    }

    @Test
    public void testPop(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);

        Assert.assertTrue( stack.pop() == 4);
        Assert.assertTrue( stack.pop() == 2);
        Assert.assertTrue( stack.pop() == null);
        Assert.assertTrue( stack.size() == 0);
    }

    @Test
    public void testTip(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);

        Assert.assertTrue( stack.tip() == 4);
        Assert.assertTrue( stack.tip() == 4);
        Assert.assertTrue( stack.size() == 2);
    }

    @Test
    public void testToString(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);

        Assert.assertTrue( "[4,2]".equals(stack.toString()) );
    }

    @Test
    public void testToStringWithEmptyStack(){
        Stack<Integer> stack = new Stack<Integer>();

        Assert.assertTrue( "[]".equals(stack.toString()) );
    }
}
